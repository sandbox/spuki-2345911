DESCRIPTION
-----------

Commerce User Profile Pane is a module that allows you to view a Drupal
Users profile fields directly in the Commerce checkout pages.

HOW IT WORKS
------------

This pane generate a block with the fields that are configured to be displayed
here. After installing this module you will find a new pane in commerce checkout config.

INSTALLATION
------------

- Enable this module as usual.

- Finally go to admin/commerce/config/checkout and define which checkout page
  the Commerce User Profile Pane should display.

CREDITS
-------

- The development of this module was sponsored by `Sanumway <http://www.sanumway.ru>`
